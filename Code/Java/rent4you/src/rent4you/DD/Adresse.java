/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rent4you.DD;

/**
 *
 * @author drznathou
 */
public class Adresse
{
    private int numero, codePostal, idAdresse;
    private String rue, ville;

    public Adresse(int numero, int codePostal, int idAdresse, String rue, String ville)
    {
        this.numero = numero;
        this.codePostal = codePostal;
        this.idAdresse = idAdresse;
        this.rue = rue;
        this.ville = ville;
    }
    
    public void modifier()
    {
    }
    

    /**
     * @return the numero
     */
    public int getNumero()
    {
        return numero;
    }

    /**
     * @return the codePostal
     */
    public int getCodePostal()
    {
        return codePostal;
    }

    /**
     * @return the rue
     */
    public String getRue()
    {
        return rue;
    }

    /**
     * @return the ville
     */
    public String getVille()
    {
        return ville;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    /**
     * @param codePostal the codePostal to set
     */
    public void setCodePostal(int codePostal)
    {
        this.codePostal = codePostal;
    }

    /**
     * @param rue the rue to set
     */
    public void setRue(String rue)
    {
        this.rue = rue;
    }

    /**
     * @param ville the ville to set
     */
    public void setVille(String ville)
    {
        this.ville = ville;
    }
    
    
}
