/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rent4you.DD;

/**
 *
 * @author drznathou
 */
public class Client extends Adresse
{
    private String nom, prenom, idNational, telephone, tva, compteBanquaire, www, email;

    public Client(String nom, String prenom, String idNational, String telephone, String tva, String compteBanquaire, String www, String email, int numero, int codePostal, int idAdresse, String rue, String ville)
    {
        super(numero, codePostal, idAdresse, rue, ville);
        this.nom = nom;
        this.prenom = prenom;
        this.idNational = idNational;
        this.telephone = telephone;
        this.tva = tva;
        this.compteBanquaire = compteBanquaire;
        this.www = www;
        this.email = email;
    }

    @Override
    public void modifier()
    {
        
    }
    
    

    /**
     * @return the nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom()
    {
        return prenom;
    }

    /**
     * @return the idNational
     */
    public String getIdNational()
    {
        return idNational;
    }

    /**
     * @return the telephone
     */
    public String getTelephone()
    {
        return telephone;
    }

    /**
     * @return the tva
     */
    public String getTva()
    {
        return tva;
    }

    /**
     * @return the compteBanquaire
     */
    public String getCompteBanquaire()
    {
        return compteBanquaire;
    }

    /**
     * @return the www
     */
    public String getWww()
    {
        return www;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * @param idNational the idNational to set
     */
    public void setIdNational(String idNational)
    {
        this.idNational = idNational;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    /**
     * @param tva the tva to set
     */
    public void setTva(String tva)
    {
        this.tva = tva;
    }

    /**
     * @param compteBanquaire the compteBanquaire to set
     */
    public void setCompteBanquaire(String compteBanquaire)
    {
        this.compteBanquaire = compteBanquaire;
    }

    /**
     * @param www the www to set
     */
    public void setWww(String www)
    {
        this.www = www;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    
    
}
