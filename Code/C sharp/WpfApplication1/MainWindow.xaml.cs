﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Fournisseur_Click(object sender, RoutedEventArgs e)
        {
            pbar.Value = 100;
        }

        private void Clients_Click(object sender, RoutedEventArgs e)
        {
            pbar.Value = 50;
        }

        private void Accueil_Click(object sender, RoutedEventArgs e)
        {
            pbar.Value = 0;
        }
    }
}
