﻿using System.Collections.Generic;

namespace rent4you.Models
{
    class ViewModelLocal : ViewModelBase
    {
        public List<Local> localList { get; set; }
    }
}
