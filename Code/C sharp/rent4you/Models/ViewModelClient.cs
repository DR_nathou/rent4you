﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rent4you.Models
{
    class ViewModelClient : ViewModelBase
    {
        public List<Client> clientList { get; set; }

        public List<Local> proprietaireLocalList { get; set; }

        public Client proprietaireEnCours { get; set; }
    }
}
