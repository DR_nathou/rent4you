﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rent4you.Models
{
    class ViewModelContrat : ViewModelBase
    {
        public List<double> hoursList { get; set; }

        public List<Contrat> contractList { get; set; }

        //public string summaryClient { get; set; }

        public  bool clientSelectionMode{ get; set; }

        public  bool localSelectionMode{ get; set; }

        public  Client selectedClient{ get; set; }

        public  Local selectedLocal{ get; set; }


        public ViewModelContrat() : base() { }
    }
}
