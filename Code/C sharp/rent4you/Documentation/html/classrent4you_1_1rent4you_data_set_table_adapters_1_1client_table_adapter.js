var classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter =
[
    [ "clientTableAdapter", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a3a37c514bf0bab5e1f2222bfaf82bdee", null ],
    [ "Delete", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#acb38d54604ade8582bb34c9c1b9782a0", null ],
    [ "Fill", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a28619d69ac0db42fdbbc80afe862f966", null ],
    [ "GetData", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#acf057ba23c17293844e8fb0a1eec2b58", null ],
    [ "Insert", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#ac9c63cf685f23c1662eea9faa1682f22", null ],
    [ "Update", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a15d503d9f6396eba4eb3580c3c2e8890", null ],
    [ "Update", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#abe9105a65fa319e16ef4df9cf4f55985", null ],
    [ "Update", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a36b3f2d7de3de16304f77cc80a1b078b", null ],
    [ "Update", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a9c0edb744404e7d715b80e7005b22041", null ],
    [ "Update", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#af4e0ba3c9b6a236bfa0001aa9fbbaca6", null ],
    [ "Update", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#aba3e1ee2296c6127a579dac8c3f614d6", null ],
    [ "Adapter", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a62fac45993b8c5641dc0342a3b31cb14", null ],
    [ "ClearBeforeFill", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#ab9a65c4889401addeacb19dbd73746c7", null ],
    [ "CommandCollection", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a8b76de3d8a55e0c3b53f1a9d901b4d47", null ],
    [ "Connection", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a53efcc602418939bcc679bae06335e5e", null ],
    [ "Transaction", "classrent4you_1_1rent4you_data_set_table_adapters_1_1client_table_adapter.html#a58f74ebfea0415983af5c859acd4fb6e", null ]
];