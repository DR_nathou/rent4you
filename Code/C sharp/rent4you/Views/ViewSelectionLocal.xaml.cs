﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using lM = rent4you.BU.LocalManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewSelectionLocal.xaml
    /// </summary>
    public partial class ViewSelectionLocal : Window
    {
        Models.ViewModelLocal vM;
        /// <summary>
        /// Local selectionné en vue de la création du contrat
        /// </summary>
        public Local selectedLocal {get; set;}
        /// <summary>
        /// Fenetre de selection d'un local
        /// </summary>
        public ViewSelectionLocal()
        {
            InitializeComponent();
            vM = new Models.ViewModelLocal();
            f5();
            this.DataContext = vM;
        }
        
        private void localsmBTAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void localsmBTSelectionner_Click(object sender, RoutedEventArgs e)
        {
            Local selected = ((Local)viewListLocaux.SelectedItem);
            if (selected != null)
            {
                selectedLocal = selected;
                this.Close();
            }
            else
            {
                MessageBox.Show("Aucun local n'est selectionné!", "Erreur", MessageBoxButton.OK);
            }
        }

        private void viewListLocaux_OnDoubleClick(object sender, RoutedEventArgs e)
        {
            localsmBTSelectionner_Click(sender, e);
        }


        private void rechercheLocaux_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource itemSourceList = new CollectionViewSource() { Source = vM.localList };
            System.ComponentModel.ICollectionView Itemlist = itemSourceList.View;

            var predicate = new Predicate<object>(item => ((Local)item).description.Contains(rechercheLocaux.Text));

            Itemlist.Filter = predicate;

            viewListLocaux.ItemsSource = Itemlist;

        }

        private void f5()
        {
            vM.localList = lM.localList();
            viewListLocaux.ItemsSource = vM.localList;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
    }
}
