﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using lM = rent4you.BU.LocalManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewLocalCreate.xaml
    /// </summary>
    public partial class ViewLocalCreate : Window
    {
        /// <summary>
        /// id du proprietaire du local en ajout dans cette fenetre
        /// </summary>
        public int ProprioId { get; set; }
        /// <summary>
        /// fenetre d'insertion d'un nouveau local
        /// </summary>
        public ViewLocalCreate()
        {
            InitializeComponent();
        }
        
        private void localBTAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void localBTEnregistrer_Click(object sender, RoutedEventArgs e)
        { 
            changeBorderTBBackTNormal();

            Local local = new Local();
            local.Adresse = new Adresse();
            int numero, codePostal, surface, loyer, forfait;

            bool numError = Int32.TryParse(localTBNumero.Text, out numero);
            bool cpError = Int32.TryParse(localTBCodePostal.Text, out codePostal);
            bool surError = Int32.TryParse(localTBSurface.Text, out surface);
            bool loError = Int32.TryParse(localTBLoyer.Text, out loyer);
            bool foError = Int32.TryParse(localTBForfait.Text, out forfait);
            bool emptyError = localExceptionHandler(numError, cpError, surError, loError, foError);



            if (numError && cpError && surError && loError && foError && !emptyError)
            {

                local.idProprietaire = ProprioId;
                local.Adresse.numero = numero;
                local.Adresse.rue = localTBRue.Text;
                local.Adresse.ville = localTBVille.Text;
                local.Adresse.pays = localTBPays.Text;
                local.Adresse.codePostal = codePostal;
                local.description = localTBDescription.Text;
                local.meuble = localCBMeuble.IsChecked.GetValueOrDefault();
                local.suprime = false;
                local.surface = surface;
                local.loyer = loyer;
                local.forfait = forfait;
                if (!emptyError) localExceptionHandler(numError, cpError, surError, loError, foError);

                MessageBox.Show(lM.insert(local), "nice", MessageBoxButton.OK);

                this.Close();
                
            }
        }

        private void changeBorderTBBackTNormal()
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(this))
            {
                tb.BorderBrush = Brushes.DarkSlateBlue;
            }
        }
        private bool localExceptionHandler(bool numError, bool cpError, bool surError, bool loError, bool foError)
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(this))
            {
                tb.BorderBrush = Brushes.LawnGreen;
            }
            List<string> er = new List<string>();
            if (localTBRue.Text == "")
            { localTBRue.BorderBrush = Brushes.Red; er.Add("rue"); }
            if (localTBNumero.Text == "" || !numError)
            { localTBNumero.BorderBrush = Brushes.Red; er.Add("Numero"); }
            if (localTBCodePostal.Text == "" || !cpError)
            { localTBCodePostal.BorderBrush = Brushes.Red; er.Add("Code Postal"); }
            if (localTBVille.Text == "")
            { localTBVille.BorderBrush = Brushes.Red; er.Add("Ville"); }
            if (localTBDescription.Text == "")
            { localTBDescription.BorderBrush = Brushes.Red; er.Add("Description"); }
            if (localTBLoyer.Text == "" || !loError)
            { localTBLoyer.BorderBrush = Brushes.Red; er.Add("Loyer"); }
            if (localTBForfait.Text == "" || !loError)
            { localTBForfait.BorderBrush = Brushes.Red; er.Add("Forfait"); }
            if (localTBSurface.Text == "" || !surError)
            { localTBSurface.BorderBrush = Brushes.Red; er.Add("Surface"); }
            if (localTBPays.Text == "")
            { localTBPays.BorderBrush = Brushes.Red; er.Add("Pays"); }

            msgBoxDotShow(er);

            return er.Count > 0;
        }

        private void msgBoxDotShow(List<string> er)
        {
            if (er.Count > 0) MessageBox.Show("Entrée(s) suivante(s) incorect: " + String.Join(", ", er));
        }

        private static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
    }
}
