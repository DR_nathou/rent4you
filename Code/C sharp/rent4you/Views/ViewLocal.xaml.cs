﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using lM = rent4you.BU.LocalManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewLocal.xaml
    /// </summary>
    public partial class ViewLocal : UserControl
    {
        private Models.ViewModelLocal vM;

        /// <summary>
        /// ViewLocal constructeur
        /// </summary>
        public ViewLocal()
        {
            InitializeComponent();
            vM = new Models.ViewModelLocal();
            PopulateAndBind();
        }

        private void PopulateAndBind()
        {
            f5();

            DataContext = vM;
        }
        private void rechercheLocaux_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource itemSourceList = new CollectionViewSource() { Source = vM.localList };
            System.ComponentModel.ICollectionView Itemlist = itemSourceList.View;

            var predicate = new Predicate<object>(item => ((Local)item).description.Contains(rechercheLocaux.Text));

            Itemlist.Filter = predicate;

            viewListLocaux.ItemsSource = Itemlist;

        }
        private void viewListLocaux_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var localSelectionne = (Local)viewListLocaux.SelectedItem;
            if (localSelectionne != null)
            {
                localTBRue.Text = localSelectionne.Adresse.rue;
                localTBNumero.Text = localSelectionne.Adresse.numero.ToString();
                localTBCodePostal.Text = localSelectionne.Adresse.codePostal.ToString();
                localTBVille.Text = localSelectionne.Adresse.ville;
                localTBDescription.Text = localSelectionne.description;
                localCBMeuble.IsChecked = localSelectionne.meuble;
                localTBLoyer.Text = localSelectionne.loyer.ToString();
                localTBForfait.Text = localSelectionne.forfait.ToString();
                localTBSurface.Text = localSelectionne.surface.ToString();
                localTBPays.Text = localSelectionne.Adresse.pays;
                localBTInfoProprietaire.Content = "Info Proprio";
            }

        }

        private void localBTEnregistrer_Click(object sender, RoutedEventArgs e)
        {
            if (viewListLocaux.SelectedItem != null)
            {
                changeBorderTBBackTNormal();

                Local local = new Local();
                local.Adresse = new Adresse();
                int numero, codePostal, surface, loyer, forfait;

                bool numError = Int32.TryParse(localTBNumero.Text, out numero);
                bool cpError = Int32.TryParse(localTBCodePostal.Text, out codePostal);
                bool surError = Int32.TryParse(localTBSurface.Text, out surface);
                bool loError = Int32.TryParse(localTBLoyer.Text, out loyer);
                bool foError = Int32.TryParse(localTBForfait.Text, out forfait);
                bool emptyError = localExceptionHandler(numError, cpError, surError, loError, foError);



                if (numError && cpError && surError && loError && foError && !emptyError)
                {
                    local.Adresse.idAdresse = ((Local)viewListLocaux.SelectedItem).Adresse.idAdresse;
                    local.idlocal = ((Local)viewListLocaux.SelectedItem).idlocal;
                    local.idProprietaire = ((Local)viewListLocaux.SelectedItem).idProprietaire;

                    local.idAdresse = local.Adresse.idAdresse;
                    local.Adresse.numero = numero;
                    local.Adresse.rue = localTBRue.Text;
                    local.Adresse.ville = localTBVille.Text;
                    local.Adresse.pays = localTBPays.Text;
                    local.Adresse.codePostal = codePostal;
                    local.description = localTBDescription.Text;
                    local.meuble = localCBMeuble.IsChecked.GetValueOrDefault();
                    local.suprime = false;
                    local.surface = surface;
                    local.loyer = loyer;
                    local.forfait = forfait;
                    lM.update(local);
                    f5();
                }
                else localExceptionHandler(numError, cpError, surError, loError, foError);
            }
            else MessageBox.Show("veuiller selectionner un local à modifier", "Erreur", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
        private void localBTNouveau_Click(object sender, RoutedEventArgs e)
        {
            changeBorderTBBackTNormal();
            viewListLocaux.UnselectAll();
            viewListLocaux.UnselectAllCells();

            adaptLocalForm(false);
        }

        private void BTCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void localBTInfoProprietaire_Click(object sender, RoutedEventArgs e)
        {
            var localSelectionne = (Local)viewListLocaux.SelectedItem;
            if(localSelectionne != null)
            {
                localBTInfoProprietaire.Content = lM.getProprietaireName(localSelectionne.idProprietaire);
            }

            else
                MessageBox.Show("aucun local n'est selectionné");

        }
        private void voirSuprime_Click(object sender, RoutedEventArgs e)
        {
            if (((Local)viewListLocaux.Items[0]).suprime)
            {
                localBTVoirSuprime.Content = "Voir les locaux supprimés";
                f5();
                adaptLocalForm(false);
            }
            else
            {
                localBTVoirSuprime.Content = "Voir tout les locaux";
                f5(true); // true
                adaptLocalForm(true);
            }
        }

        private void adaptLocalForm(bool localSuprimme)
        {
            if (localSuprimme)
            {
                localTBCodePostal.IsEnabled = false;
                localTBDescription.IsEnabled = false;
                localTBForfait.IsEnabled = false;
                localTBLoyer.IsEnabled = false;
                localTBNumero.IsEnabled = false;
                localTBRue.IsEnabled = false;
                localTBSurface.IsEnabled = false;
                localTBVille.IsEnabled = false;
                localCBMeuble.IsEnabled = false;
                localTBPays.IsEnabled = false;
                localBTSuprime.Content = "Réactiver";
            }
            else
            {
                localTBCodePostal.IsEnabled = true;
                localTBDescription.IsEnabled = true;
                localTBForfait.IsEnabled = true;
                localTBLoyer.IsEnabled = true;
                localTBNumero.IsEnabled = true;
                localTBRue.IsEnabled = true;
                localTBSurface.IsEnabled = true;
                localTBVille.IsEnabled = true;
                localCBMeuble.IsEnabled = true;
                localTBPays.IsEnabled = true;
                localBTSuprime.Content = "Supprimer";
            }

        }


        private void suprimerlocal(object sender, RoutedEventArgs e)
        {
            try
            {
                var localSelectionne = ((Local)viewListLocaux.SelectedItem);
                if (localSelectionne.suprime)
                {
                    lM.delete(localSelectionne.idlocal, false);
                    f5(true); //true
                    MessageBox.Show("Local réactivé " + localSelectionne.description, "Message");
                }
                else
                {
                    lM.delete(localSelectionne.idlocal, true);
                    f5();
                    MessageBox.Show("Local suprimé " + localSelectionne.description, "Message");
                }
            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("Aucun local n'est selectionné");
            }
        }
        private void changeBorderTBBackTNormal()
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(this))
            {
                tb.BorderBrush = Brushes.DarkSlateBlue;
            }
        }

        private void f5(bool del = false)//bool del)
        {
            if (del)
            {
                vM.localList = lM.localSupprimeList();
                viewListLocaux.ItemsSource = vM.localList;
            }
            else
            {
                vM.localList = lM.localList();
                viewListLocaux.ItemsSource = vM.localList;
            }
            
        }
        private static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        private bool localExceptionHandler(bool numError, bool cpError, bool surError, bool loError, bool foError)
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(this))
            {
                tb.BorderBrush = Brushes.LawnGreen;
            }
            List<string> er = new List<string>();
            if (localTBRue.Text == "")
            { localTBRue.BorderBrush = Brushes.Red; er.Add("rue"); }
            if (localTBNumero.Text == "" || !numError)
            { localTBNumero.BorderBrush = Brushes.Red; er.Add("Numero"); }
            if (localTBCodePostal.Text == "" || !cpError)
            { localTBCodePostal.BorderBrush = Brushes.Red; er.Add("Code Postal"); }
            if (localTBVille.Text == "")
            { localTBVille.BorderBrush = Brushes.Red; er.Add("Ville"); }
            if (localTBDescription.Text == "")
            { localTBDescription.BorderBrush = Brushes.Red; er.Add("Description"); }
            if (localTBLoyer.Text == "" || !loError)
            { localTBLoyer.BorderBrush = Brushes.Red; er.Add("Loyer"); }
            if (localTBForfait.Text == "" || !loError)
            { localTBForfait.BorderBrush = Brushes.Red; er.Add("Forfait"); }
            if (localTBSurface.Text == "" || !surError)
            { localTBSurface.BorderBrush = Brushes.Red; er.Add("Surface"); }
            if (localTBPays.Text == "")
            { localTBPays.BorderBrush = Brushes.Red; er.Add("Pays"); }

            msgBoxDotShow(er);

            return er.Count > 0;
        }
        private void msgBoxDotShow(List<string> er)
        {
            if (er.Count > 0) MessageBox.Show("Entrée(s) suivante(s) incorect: " + String.Join(", ", er));
        }

        private void UserControl_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            PopulateAndBind();
        }
    }
}
