﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using cM = rent4you.BU.ClientManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewSelectionClient.xaml
    /// </summary>
    public partial class ViewSelectionClient : Window
    {
        /// <summary>
        /// reference le client selectionné
        /// </summary>
        public Client selectedClient { get; set; }
        Models.ViewModelClient vM;
        /// <summary>
        /// instantie une fenetre de selection d'un client
        /// </summary>
        public ViewSelectionClient()
        {
            InitializeComponent();
            vM = new Models.ViewModelClient();
            f5();
            this.DataContext = vM;

        }
        
        private void clientsmBTAnnuler_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void clientsmBTSelectionner_Click(object sender, RoutedEventArgs e)
        {
            Client selected = ((Client)viewListClients.SelectedItem);
            if (selected != null)
            {
                selectedClient = selected;
                this.Close();
            }
            else
            {
                MessageBox.Show("Aucun client n'est selectionné!", "Erreur", MessageBoxButton.OK);
            }
        }
        private void viewListClients_OnDoubleClick(object sender, RoutedEventArgs e)
        {
            clientsmBTSelectionner_Click(sender, e);
        }

        private void rechercheClients_TextChanged(object sender, TextChangedEventArgs e)
        {
            String t = rechercheClients.Text;
            //if (t.Count() > 1) t = t.First().ToString().ToUpper();

            CollectionViewSource itemSourceList = new CollectionViewSource() { Source = vM.clientList };
            System.ComponentModel.ICollectionView Itemlist = itemSourceList.View;

            var predicate = new Predicate<object>(item => ((Client)item).nom.Contains(t));

            Itemlist.Filter = predicate;

            viewListClients.ItemsSource = Itemlist;
        }

        private void clientViewBTTous_Click(object sender, RoutedEventArgs e)
        {
            f5();
        }

        private void clientViewBTClient_Click(object sender, RoutedEventArgs e)
        {
            filter(0);
        }
        private void clientViewBTPropriétaire_Click(object sender, RoutedEventArgs e)
        {
            filter(1);
        }
        
        private void f5()
        {
            vM.clientList = cM.clientList();
            viewListClients.ItemsSource = vM.clientList;
        }

        private void filter(int typeDeClient)
        {
            CollectionViewSource itemSourceList = new CollectionViewSource() { Source = vM.clientList };
            System.ComponentModel.ICollectionView Itemlist = itemSourceList.View;

            var predicate = new Predicate<object>(item => ((Client)item).typeDeClient == typeDeClient);

            Itemlist.Filter = predicate;

            viewListClients.ItemsSource = Itemlist;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
        
    }
}
