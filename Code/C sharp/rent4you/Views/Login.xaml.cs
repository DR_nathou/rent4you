﻿using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using authMgr = rent4you.BU.AuthManager;
using sM = rent4you.BU.SessionManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        /// <summary>
        /// Instantie une fenetre permetant l'acces aux fonctionnalités de l'application
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }

        private void connection_Click(object sender, RoutedEventArgs e)
        {
            Auth login = new Auth();
            login.identifiant = identifiant.Text;
            login.motDePasse = this.hashDuMotDePasse(motDePasse.Password.ToString());

            progress.Visibility = Visibility.Visible;
            connection.IsEnabled = false;
            nouvelUtilisateur.IsEnabled = false;
            identifiant.IsEnabled = false;
            motDePasse.IsEnabled = false;

            Common.CancelableBackgroundWorker bw = new Common.CancelableBackgroundWorker();
            
            bw.WorkerReportsProgress = true;

            //set des instructions a effectuer par le bw
            bw.DoWork += new DoWorkEventHandler(
            delegate (object o, DoWorkEventArgs args)
            {
                Common.CancelableBackgroundWorker b = o as Common.CancelableBackgroundWorker;

                MessageBox.Show(authMgr.login(login), "", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                b.Abort();
            });
            
            //Event surveillé lors de la terminaison du bw thread
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
            delegate (object o, RunWorkerCompletedEventArgs args)
            {
                if (sM.currentSession() != null)
                {
                    new Views.ViewMainWindow().Show();
                    this.Close();
                }
                else
                {
                    progress.Visibility = Visibility.Hidden;
                    connection.IsEnabled = true;
                    nouvelUtilisateur.IsEnabled = true;
                    identifiant.IsEnabled = true;
                    motDePasse.IsEnabled = true;
                }

            });

            bw.RunWorkerAsync();
        }
        private void nouvelUtilisateur_Click(object sender, RoutedEventArgs e)
        {
            Auth auth = new Auth();
            auth.identifiant = identifiant.Text;
            auth.motDePasse = hashDuMotDePasse(motDePasse.Password.ToString());
            if (authMgr.register(auth))
            {
                MessageBox.Show("Identifiants ajoutés avec succes", "Nice", MessageBoxButton.OK, MessageBoxImage.Information);
                connection_Click(sender, e);
            }
            else
                MessageBox.Show("identifiant déjà utilisé. Veuillez en choisir un autre", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
            
        }
        private void identifiant_GotFocus(object sender, RoutedEventArgs e)
        {
            identifiant.SelectAll();
        }
        private void motDePasse_GotFocus(object sender, RoutedEventArgs e)
        {
            motDePasse.SelectAll();
        }
        private string hashDuMotDePasse(string mdp)
        {
            var hash = (new System.Security.Cryptography.SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(mdp));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }
    }
}
