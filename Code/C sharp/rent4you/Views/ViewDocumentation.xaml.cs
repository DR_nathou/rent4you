﻿using System.Windows.Controls;
using dM = rent4you.BU.DocumentationManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewDocumentation.xaml
    /// </summary>
    public partial class ViewDocumentation : UserControl
    {
        Models.ViewModelDocumentation vM;
        /// <summary>
        /// 
        /// </summary>
        public ViewDocumentation()
        {
            InitializeComponent();
            vM = new Models.ViewModelDocumentation();
            this.DataContext = vM;
            vM.url = dM.url();
            browser.Navigate(vM.url);
        }
    }
}
