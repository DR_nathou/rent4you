﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ctM = rent4you.BU.ContractManager;
using lM = rent4you.BU.LocalManager;
using cM = rent4you.BU.ClientManager;
using System.Threading;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewContrat.xaml
    /// </summary>
    public partial class ViewContrat : UserControl
    {
        private Models.ViewModelContrat vM { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ViewContrat()
        {
            InitializeComponent();
            vM = new Models.ViewModelContrat();
            PopulateAndBind();
        }
        


        private void PopulateAndBind()
        {
            f5();
            
            vM.hoursList = new List<double>();

            for (double i = 0; i < 24; i += 0.5)
            {
                vM.hoursList.Add(i);
            }
            this.DataContext = vM;
        }

        private void contractBTPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var yo = (Contrat)viewListContracts.SelectedItem;
                ctM.generatePDF(yo);
            }
            catch (Exception ee)
            {
                MessageBox.Show("no contrat selected \n" + ee);
            }
        }

        private void contractBTSelectClient_Click(object sender, RoutedEventArgs e)
        {
            vM.clientSelectionMode = true;
            vM.selectedClient = null;
            var selectClientWindow = new ViewSelectionClient();

            if(!selectClientWindow.ShowDialog().GetValueOrDefault())
            {
                vM.selectedClient = selectClientWindow.selectedClient;
                contractTBSummaryLocataire.Text = cM.describeYourself(vM.selectedClient);
            }
            
                
        }

        private void contractBTSelectLocal_Click(object sender, RoutedEventArgs e)
        {
            vM.localSelectionMode = true;
            vM.selectedLocal = null;
            var selectLocalWindow = new ViewSelectionLocal();

            if (!selectLocalWindow.ShowDialog().GetValueOrDefault())
            {
                vM.selectedLocal = selectLocalWindow.selectedLocal;
                contractTBSummaryLocal.Text = lM.describeYourself(vM.selectedLocal);
            }
        }

        private void viewListContracts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var contratSelectionne = (Contrat)viewListContracts.SelectedItem;
            if (contratSelectionne != null)
            {
                contractTBSummaryLocataire.Text = cM.describeYourself(contratSelectionne.Client);
                contractTBSummaryLocal.Text = lM.describeYourself(contratSelectionne.Local);
                contractTBSummaryPeriode.Text = ctM.describeYourself(contratSelectionne);

                contractBTSelectClient.IsEnabled = false;
                contractBTSelectLocal.IsEnabled = false;
                contractCBForfait.IsEnabled = false;
                contractCBTimeDeb.IsEnabled = false;
                contractCBTimeFin.IsEnabled = false;
                contractDPDateDeb.IsEnabled = false;
                contractDPDateFin.IsEnabled = false;
                contractBTValider.IsEnabled = false;
            }
        }

        private void BTCancel_Click(object sender, RoutedEventArgs e)
        {
            vM.selectedClient = null;
            vM.selectedLocal = null;
            vM.clientSelectionMode = false;
            vM.localSelectionMode = false;
        }

        private void contractBTNouveau_Click(object sender, RoutedEventArgs e)
        {
            contractBTSelectClient.IsEnabled = true;
            contractBTSelectLocal.IsEnabled = true;
            viewListContracts.IsEnabled = false;
            contractCBForfait.IsEnabled = true;
            contractCBTimeDeb.IsEnabled = true;
            contractCBTimeFin.IsEnabled = true;
            contractDPDateDeb.IsEnabled = true;
            contractDPDateFin.IsEnabled = true;
            contractBTValider.IsEnabled = true;
            contractBTAnnuler.IsEnabled = true;
            contractTBSummaryLocal.Text = "";
            contractTBSummaryLocataire.Text = "";
            contractTBSummaryPeriode.Text = "";
        }

        private void contractCBForfait_Checked(object sender, RoutedEventArgs e)
        {
            if (contractCBForfait.IsChecked.GetValueOrDefault())
            {
                contractCBTimeDeb.IsEnabled = false;
                contractCBTimeFin.IsEnabled = false;
            }
            else
            {
                contractCBTimeDeb.IsEnabled = true;
                contractCBTimeFin.IsEnabled = true;
            }
        }

        private void contractBTValider_Click(object sender, RoutedEventArgs e)
        {
            List<string> error = new List<string>();

            Contrat contrat = new Contrat();

            if (vM.selectedClient == null)
            {
                error.Add("Veuillez selectionner un client");
            } else
                contrat.idClient = vM.selectedClient.idClient;
            if (vM.selectedLocal == null)
            {
                error.Add("Veuillez selectionner un local");
            } else
                contrat.idLocal = vM.selectedLocal.idlocal;

           
            
            contrat.dateSignature = DateTime.Now;

            contrat.dateDebut = contractDPDateDeb.SelectedDate.GetValueOrDefault();
            contrat.dateFin = contractDPDateFin.SelectedDate.GetValueOrDefault();

            if (contrat.dateDebut == new DateTime(0001, 01, 01)) error.Add("Veuiller selectionner une date de début de contrat");
            if (contrat.dateFin == new DateTime(0001, 01, 01)) error.Add("Veuiller selectionner une date de fin de contrat");

            if (contrat.dateDebut > contrat.dateFin)
            {
                error.Add("Impossible de selectionner une date antérieur");
            }
            if (contractCBTimeDeb.SelectedIndex != -1 && contractCBForfait.IsChecked.GetValueOrDefault() != true)
                contrat.dateDebut = contrat.dateDebut.AddHours(Convert.ToDouble(vM.hoursList[contractCBTimeDeb.SelectedIndex]));
            else if (contractCBTimeDeb.SelectedIndex == -1 && contractCBForfait.IsChecked.GetValueOrDefault() != true)
            {
                error.Add("Veuiller selectionner une heure pour le début de la location");
            }
            if (contractCBTimeFin.SelectedIndex != -1 && contractCBForfait.IsChecked.GetValueOrDefault() != true)
                contrat.dateFin = contrat.dateFin.AddHours(Convert.ToDouble(vM.hoursList[contractCBTimeFin.SelectedIndex]));
            else if (contractCBTimeFin.SelectedIndex == -1 && contractCBForfait.IsChecked.GetValueOrDefault() != true)
            {
                error.Add("Veuiller selectionner une heure pour la fin de la location");
            }

            if (error.Count > 0)
            {
                contractErrorHandler(error);
            }
            else
            {
                contrat.coutTotal = ctM.calculateTotalPrice((contrat.dateFin - contrat.dateDebut), contrat.idLocal);
                f5();
                MessageBox.Show(ctM.insertNewContrat(contrat));
                contractBTAnnuler_Click(sender, e);

            }
        }

        private void contractBTAnnuler_Click(object sender, RoutedEventArgs e)
        {
            contractBTSelectClient.IsEnabled = false;
            contractBTSelectLocal.IsEnabled = false;
            viewListContracts.IsEnabled = true;
            contractCBForfait.IsEnabled = false;
            contractCBTimeDeb.IsEnabled = false;
            contractCBTimeFin.IsEnabled = false;
            contractDPDateDeb.IsEnabled = false;
            contractDPDateFin.IsEnabled = false;
            contractBTValider.IsEnabled = false;
            contractBTAnnuler.IsEnabled = false;
            vM.clientSelectionMode = false;
            vM.localSelectionMode = false;
            vM.selectedClient = null;
            vM.selectedLocal = null;
        }

        private void contractErrorHandler(List<string> error)
        {
            MessageBox.Show(String.Join("\r ", error));
        }

        private void rechercheContrat_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource itemSourceList = new CollectionViewSource() { Source = vM.contractList };
            System.ComponentModel.ICollectionView Itemlist = itemSourceList.View;

            var predicate = new Predicate<object>(item => ((Contrat)item).Client.nom.Contains(rechercheContrat.Text));
            var predicatePrenom = new Predicate<object>(item => ((Contrat)item).Client.prenom.Contains(rechercheContrat.Text));
            Itemlist.Filter = predicate + predicatePrenom;

            viewListContracts.ItemsSource = Itemlist;
        }
        private void f5()
        {
            vM.contractList = ctM.contractList(); 
            viewListContracts.ItemsSource = vM.contractList;
        }
    }

}

