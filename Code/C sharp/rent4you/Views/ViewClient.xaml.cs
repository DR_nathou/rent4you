﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using cM = rent4you.BU.ClientManager;
using lM = rent4you.BU.LocalManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour ViewClient.xaml
    /// </summary>
    public partial class ViewClient : UserControl
    {
        Models.ViewModelClient vM;
        /// <summary>
        /// /
        /// </summary>
        public ViewClient()
        {
            InitializeComponent();
            vM = new Models.ViewModelClient();
            PopulateAndBind();
        }
        private void PopulateAndBind()
        {
            f5();
            this.DataContext = vM;
        }
        private void rechercheClients_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource itemSourceList = new CollectionViewSource() { Source = vM.clientList };
            System.ComponentModel.ICollectionView Itemlist = itemSourceList.View;

            var predicate = new Predicate<object>(item => ((Client)item).nom.Contains(rechercheClients.Text));

            Itemlist.Filter = predicate;

            viewListClients.ItemsSource = Itemlist;

        }
        private void viewListClients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clientBTAddLocal.Visibility = Visibility.Visible;
            var clientSelectionne = (Client)viewListClients.SelectedItem;
            if (clientSelectionne != null)
            {
                PopulateTB(clientSelectionne);
                //if (clientSelectionne.Local.Count > 0) viewListLocauxProprietaires.Visibility = Visibility.Visible;
                //else viewListLocauxProprietaires.Visibility = Visibility.Hidden;
            }

        }

        private void PopulateTB(Client c)
        {
            clientTBRue.Text = c.Adresse.rue;
            clientTBNumero.Text = c.Adresse.numero.ToString();
            clientTBCodePostal.Text = c.Adresse.codePostal.ToString();
            clientTBVille.Text = c.Adresse.ville;
            clientTBPays.Text = c.Adresse.pays;
            clientTBEmail.Text = c.email;
            clientTBIdNational.Text = c.idNational;
            clientTBTelephone.Text = c.telephone;
            clientTBWWW.Text = c.www;
            clientTBNom.Text = c.nom;
            clientTBPrenom.Text = c.prenom;
            clientTBTva.Text = c.tva;
            vM.proprietaireLocalList = cM.proprietaireLocalList(c.idClient);
            viewListLocauxProprietaires.ItemsSource = vM.proprietaireLocalList;

        }

        private void clientBTEnregistrer_Click(object sender, RoutedEventArgs e)
        {

            changeBorderTBBackTNormal();
            Client client = new Client();
            client.Adresse = new Adresse();
            int numero, codePostal;
            bool numError = Int32.TryParse(clientTBNumero.Text, out numero);
            bool cpError = Int32.TryParse(clientTBCodePostal.Text, out codePostal);
            bool emptyError = clientExceptionHandler(numError, cpError);



            if (numError && cpError && !emptyError)
            {
                client.Adresse.rue = clientTBRue.Text;
                client.Adresse.numero = numero;
                client.Adresse.codePostal = codePostal;
                client.Adresse.ville = clientTBVille.Text;
                client.Adresse.pays = clientTBPays.Text;
                if (viewListClients.SelectedItem != null)
                {
                    client.Adresse.idAdresse = ((Client)viewListClients.SelectedItem).Adresse.idAdresse;
                    client.idAdresse = ((Client)viewListClients.SelectedItem).Adresse.idAdresse;
                    client.idClient = ((Client)viewListClients.SelectedItem).idClient;
                    client.typeDeClient = ((Client)viewListClients.SelectedItem).typeDeClient;
                }
                client.nom = clientTBNom.Text;
                client.prenom = clientTBPrenom.Text;
                client.idNational = clientTBIdNational.Text;
                client.tva = clientTBTva.Text;
                client.telephone = clientTBTelephone.Text;
                
                client.www = clientTBWWW.Text;
                client.email = clientTBEmail.Text;


                if (viewListClients.SelectedItem == null) cM.insert(client);
                else cM.update(client);
                f5();

                if (!emptyError) clientExceptionHandler(numError, cpError);
            }
        }
        private bool clientExceptionHandler(bool numError, bool cpError)
        {

            foreach (TextBox tb in FindVisualChildren<TextBox>(this))
            {
                tb.BorderBrush = Brushes.LawnGreen;
            }
            List<string> er = new List<string>();
            if (clientTBRue.Text == "")
            { clientTBRue.BorderBrush = Brushes.Red; er.Add("rue"); }
            if (clientTBNumero.Text == "" || !numError)
            { clientTBNumero.BorderBrush = Brushes.Red; er.Add("Numero"); }
            if (clientTBCodePostal.Text == "" || !cpError)
            { clientTBCodePostal.BorderBrush = Brushes.Red; er.Add("Code Postal"); }
            if (clientTBVille.Text == "")
            { clientTBVille.BorderBrush = Brushes.Red; er.Add("Ville"); }
            if (clientTBPays.Text == "")
            { clientTBPays.BorderBrush = Brushes.Red; er.Add("Pays"); }
            if (clientTBNom.Text == "")
            { clientTBNom.BorderBrush = Brushes.Red; er.Add("Nom"); }
            if (clientTBPrenom.Text == "")
            { clientTBPrenom.BorderBrush = Brushes.Red; er.Add("Prenom"); }
            if (clientTBIdNational.Text == "")
            { clientTBIdNational.BorderBrush = Brushes.Red; er.Add("IdNational"); }
            if (clientTBEmail.Text == "")
            { clientTBEmail.BorderBrush = Brushes.Red; er.Add("Email"); }
            if (clientTBTelephone.Text == "")
            { clientTBTelephone.BorderBrush = Brushes.Red; er.Add("Telephone"); }
            msgBoxDotShow(er);
            return er.Count > 0;


        }

        private void clientBTNouveau_Click(object sender, RoutedEventArgs e)
        {
            changeBorderTBBackTNormal();
            viewListClients.UnselectAll();
            viewListClients.UnselectAllCells();

            clientBTAddLocal.Visibility = Visibility.Hidden; // not implemented yet

            foreach (TextBox tb in FindVisualChildren<TextBox>(this)) tb.Text = "";

        }

        private void BTCancel_Click(object sender, RoutedEventArgs e)
        {

        }
        private void clientBTAnnuler_Click(object sender, RoutedEventArgs e)
        {

        }

        private void msgBoxDotShow(List<string> er)
        {
            if (er.Count > 0) MessageBox.Show("Entrée(s) suivante(s) incorect: " + String.Join(", ", er));
        }

        private void clientBTAddLocal_Click(object sender, RoutedEventArgs e)
        {
            Client c = ((Client)viewListClients.SelectedItem);
            
            if (c != null)
            {
                var localCreateWindow = new ViewLocalCreate();
                localCreateWindow.ProprioId = c.idClient;
                localCreateWindow.ShowDialog();

                vM.proprietaireLocalList = cM.proprietaireLocalList(c.idClient);
                viewListLocauxProprietaires.ItemsSource = vM.proprietaireLocalList;
            }
            else MessageBox.Show("Veuillez selectionner un client dabord.", "Erreur", MessageBoxButton.OK, MessageBoxImage.Hand);
        }

        private void clientViewBTTous_Click(object sender, RoutedEventArgs e)
        {
            viewListClients.ItemsSource = cM.clientList();
        }

        private void clientViewBTClient_Click(object sender, RoutedEventArgs e)
        {
            viewListClients.ItemsSource = cM.clientListByTypeDeClient(0);
        }

        private void clientViewBTPropriétaire_Click(object sender, RoutedEventArgs e)
        {
            viewListClients.ItemsSource = cM.clientListByTypeDeClient(1);
        }

        private void supprimerLocalFromClient(object sender, RoutedEventArgs e)
        {
            try
            {
                var localSelectionne = ((Local)viewListLocauxProprietaires.SelectedItem);

                lM.delete(localSelectionne.idlocal, true);
                vM.proprietaireLocalList = cM.proprietaireLocalList(((Client)viewListClients.SelectedItem).idClient);
                viewListLocauxProprietaires.ItemsSource = vM.proprietaireLocalList;
                MessageBox.Show("Local suprimé " + localSelectionne.description, "Message");

            }
            catch (System.NullReferenceException)
            {
                MessageBox.Show("Aucun local n'est selectionné");
            }
        }
        private void changeBorderTBBackTNormal()
        {
            foreach (TextBox tb in FindVisualChildren<TextBox>(this))
            {
                tb.BorderBrush = Brushes.DarkSlateBlue;
            }
        }

        private void f5()
        {
            vM.clientList = cM.clientList();
            viewListClients.ItemsSource = vM.clientList;
        }

        private static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}
