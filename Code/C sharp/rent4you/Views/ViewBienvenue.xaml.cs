﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using authMgr = rent4you.BU.AuthManager;
using lM = rent4you.BU.LocalManager;
using cM = rent4you.BU.ClientManager;
using ctM = rent4you.BU.ContractManager;
using sM = rent4you.BU.SessionManager;

namespace rent4you.Views
{
    /// <summary>
    /// Logique d'interaction pour Bienvenue.xaml
    /// </summary>
    public partial class Bienvenue : UserControl
    {
        Models.ViewModelMainWindow vM;
        /// <summary>
        /// 
        /// </summary>
        
        public Bienvenue()
        {
            InitializeComponent();
            PopulateAndBind();
        }
        
        

        private void PopulateAndBind()
        {
            List<string> txt = ctM.summary();
            labelSummary0VAL.Content = txt[0];
            labelSummary1VAL.Content = txt[1];
            labelSummary2VAL.Content = txt[2];

            this.DataContext = vM;
        }

        private void validChangerMotDePasse_Click(object sender, RoutedEventArgs e)
        {
            if (motDePasseNouveau.Password == motDePasseNouveauRetape.Password)
            {
                MessageBox.Show(authMgr.updatePassword(hashDuMotDePasse(motDePasseActuel.Password.ToString()), hashDuMotDePasse(motDePasseNouveau.Password.ToString())), "", MessageBoxButton.OK);
                changerMotDePasseBtn_Click(sender, e);
            }
            else
                MessageBox.Show("Les mots de passes ne sont pas identique");
        }
        private string hashDuMotDePasse(string mdp)
        {
            var hash = (new System.Security.Cryptography.SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(mdp));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());

        }
        private void changerMotDePasseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (changerMotDePasseZone.Visibility == Visibility.Hidden) changerMotDePasseZone.Visibility = Visibility.Visible;
            else
            {
                changerMotDePasseZone.Visibility = Visibility.Hidden;
                motDePasseActuel.Clear();
                motDePasseNouveau.Clear();
                motDePasseNouveauRetape.Clear();
            }
        }
        
        //private void masqueDeLaZoneDeConnection(bool connected)
        //{
        //    foreach (var item in FindVisualChildren<TabItem>(main))
        //    {
        //        item.IsEnabled = connected;
        //    }
            
        //    if (connected)
        //    {
        //        List<string> txt = ctM.summary();
        //        seDeconnecter.Visibility = Visibility.Visible;
        //        //seConnecter.Visibility = Visibility.Hidden;
        //        changerMotDePasseBtn.Visibility = Visibility.Visible;
        //        labelSummary0.Visibility = Visibility.Visible;
        //        labelSummary1.Visibility = Visibility.Visible;
        //        labelSummary2.Visibility = Visibility.Visible;
        //        labelSummary0VAL.Visibility = Visibility.Visible;
        //        labelSummary1VAL.Visibility = Visibility.Visible;
        //        labelSummary2VAL.Visibility = Visibility.Visible;

        //        labelSummary0VAL.Content = txt[0];
        //        labelSummary1VAL.Content = txt[1];
        //        labelSummary2VAL.Content = txt[2];
        //    }

        //    else
        //    {
        //        seDeconnecter.Visibility = Visibility.Hidden;
        //        //seConnecter.Visibility = Visibility.Visible;
        //        changerMotDePasseBtn.Visibility = Visibility.Hidden;
        //        changerMotDePasseZone.Visibility = Visibility.Hidden;
        //        labelSummary0.Visibility = Visibility.Hidden;
        //        labelSummary1.Visibility = Visibility.Hidden;
        //        labelSummary2.Visibility = Visibility.Hidden;
        //        labelSummary0VAL.Visibility = Visibility.Hidden;
        //        labelSummary1VAL.Visibility = Visibility.Hidden;
        //        labelSummary2VAL.Visibility = Visibility.Hidden;
        //        //messageLabelContratResume ;
        //    }

        //}
        private void seDeconnecter_Click(object sender, RoutedEventArgs e)
        {
            sM.unValidate();
            new Login().Show();
            Window.GetWindow(this).Close();
        }


        private IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        private TextBox FindParticularControl(string name, IEnumerable<TextBox> objList)
        {
            foreach (var item in objList)
            {
                if (item.Name == name) return item;
            }
            return null;
        }
    }
}
