﻿using System.Windows;
using System.Windows.Input;
using sM = rent4you.BU.SessionManager;


namespace rent4you.Views
{
    /// <summary>
    /// 
    /// Logique d'interaction pour MainWindow.xaml
    /// <MainWindow>
    /// la main window est fantastiquement super chargée
    /// ah! plus maintenant car le contenu des tab est geré dans des users contrôles
    /// </MainWindow>
    /// Controler
    /// 
    /// </summary>
    public partial class ViewMainWindow : Window
    {
        Models.ViewModelMainWindow vM;

        /// <summary>
        /// 
        /// </summary>
        public ViewMainWindow()
        {
            if (sM.currentSession() == null) throw new Common.Exception.rent4youNotConnectedException("Veuillez vous connecter pour utiliser rent4you App");
            InitializeComponent();
            vM = new Models.ViewModelMainWindow();
            
        }

        private void add_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void Window_Closed(object sender, System.EventArgs e)
        {
            sM.unValidate();
        }
    }
}
