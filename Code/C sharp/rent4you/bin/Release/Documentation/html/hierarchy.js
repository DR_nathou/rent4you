var hierarchy =
[
    [ "Application", null, [
      [ "rent4you.App", "df/d24/classrent4you_1_1_app.html", null ]
    ] ],
    [ "BackgroundWorker", null, [
      [ "rent4you.Common.CancelableBackgroundWorker", "df/d0e/classrent4you_1_1_common_1_1_cancelable_background_worker.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "rent4you.Properties.Settings", "d4/d52/classrent4you_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "rent4you.Adresse", "df/d8a/classrent4you_1_1_adresse.html", null ],
    [ "rent4you.Auth", "d2/d9f/classrent4you_1_1_auth.html", null ],
    [ "rent4you.BU.AdresseManager", "dc/d0e/classrent4you_1_1_b_u_1_1_adresse_manager.html", null ],
    [ "rent4you.BU.AuthManager", "d5/dc0/classrent4you_1_1_b_u_1_1_auth_manager.html", null ],
    [ "rent4you.BU.ClientManager", "db/dc5/classrent4you_1_1_b_u_1_1_client_manager.html", null ],
    [ "rent4you.BU.ContractManager", "d7/dc4/classrent4you_1_1_b_u_1_1_contract_manager.html", null ],
    [ "rent4you.BU.DocumentationManager", "d4/de1/classrent4you_1_1_b_u_1_1_documentation_manager.html", null ],
    [ "rent4you.BU.LocalManager", "db/db7/classrent4you_1_1_b_u_1_1_local_manager.html", null ],
    [ "rent4you.BU.SessionManager", "de/deb/classrent4you_1_1_b_u_1_1_session_manager.html", null ],
    [ "rent4you.Client", "d4/de7/classrent4you_1_1_client.html", null ],
    [ "rent4you.Common.Constants", "d5/d63/classrent4you_1_1_common_1_1_constants.html", null ],
    [ "rent4you.Contrat", "d2/d37/classrent4you_1_1_contrat.html", null ],
    [ "rent4you.Local", "da/d4f/classrent4you_1_1_local.html", null ],
    [ "rent4you.Models.ViewModelBase", "dc/d66/classrent4you_1_1_models_1_1_view_model_base.html", [
      [ "rent4you.Models.ViewModelClient", "d0/d5b/classrent4you_1_1_models_1_1_view_model_client.html", null ],
      [ "rent4you.Models.ViewModelContrat", "dc/dc3/classrent4you_1_1_models_1_1_view_model_contrat.html", null ],
      [ "rent4you.Models.ViewModelLocal", "db/ddc/classrent4you_1_1_models_1_1_view_model_local.html", null ]
    ] ],
    [ "rent4you.Models.ViewModelDocumentation", "db/dc9/classrent4you_1_1_models_1_1_view_model_documentation.html", null ],
    [ "rent4you.Models.ViewModelMainWindow", "d1/d32/classrent4you_1_1_models_1_1_view_model_main_window.html", null ],
    [ "rent4you.Properties.Resources", "d4/d3a/classrent4you_1_1_properties_1_1_resources.html", null ],
    [ "rent4you.Session", "d9/df5/classrent4you_1_1_session.html", null ],
    [ "Exception", null, [
      [ "rent4you.Common.Exception.rent4youNotConnectedException", "df/dfa/classrent4you_1_1_common_1_1_exception_1_1rent4you_not_connected_exception.html", null ]
    ] ],
    [ "UserControl", null, [
      [ "rent4you.Views.Bienvenue", "df/d89/classrent4you_1_1_views_1_1_bienvenue.html", null ],
      [ "rent4you.Views.ViewClient", "dd/dbd/classrent4you_1_1_views_1_1_view_client.html", null ],
      [ "rent4you.Views.ViewContrat", "d6/d1d/classrent4you_1_1_views_1_1_view_contrat.html", null ],
      [ "rent4you.Views.ViewDocumentation", "d3/d86/classrent4you_1_1_views_1_1_view_documentation.html", null ],
      [ "rent4you.Views.ViewLocal", "d6/dad/classrent4you_1_1_views_1_1_view_local.html", null ]
    ] ],
    [ "Window", null, [
      [ "rent4you.Views.Login", "d8/da5/classrent4you_1_1_views_1_1_login.html", null ],
      [ "rent4you.Views.ViewLocalCreate", "d8/d77/classrent4you_1_1_views_1_1_view_local_create.html", null ],
      [ "rent4you.Views.ViewMainWindow", "d6/dd9/classrent4you_1_1_views_1_1_view_main_window.html", null ],
      [ "rent4you.Views.ViewSelectionClient", "d2/dba/classrent4you_1_1_views_1_1_view_selection_client.html", null ],
      [ "rent4you.Views.ViewSelectionLocal", "d5/d37/classrent4you_1_1_views_1_1_view_selection_local.html", null ]
    ] ]
];