var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvw",
  1: "abcdlrsv",
  2: "r",
  3: "acdlrsv",
  4: "abcdgiloprsuvw",
  5: "acdls",
  6: "acdefhilmnprstuvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "typedefs",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Typedefs",
  6: "Properties"
};

