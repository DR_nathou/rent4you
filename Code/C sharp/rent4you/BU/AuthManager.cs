﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Data.Entity.Core;
using sM = rent4you.BU.SessionManager;

namespace rent4you.BU
{
    class AuthManager
    {
               
        private static List<Auth> connectionsList()
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                return model.Auth.ToList();
            }
        }

        public static string login(Auth identifiant)
        {
            try
            {
                foreach (var id in connectionsList())
                {
                    if (id.identifiant == identifiant.identifiant)
                    {
                        if (id.motDePasse == identifiant.motDePasse)
                        {
                            sM.write(id.idAuthentification);
                            return "Bienvenue " + identifiant.identifiant;
                        }
                        else return "mot de passe incorrect!";
                    }
                }
                return "Identifiant incorrect!";
            }
            catch (EntityException)
            {
                return "Erreur de connection avec la base de données";
            }
        }
        public static bool register(Auth Auth)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                try
                {
                    foreach (var item in AuthManager.connectionsList())
                    {
                        if (item.identifiant == Auth.identifiant) return false;
                    }
                    model.Auth.Add(Auth);
                    model.SaveChanges();
                    return true;
                }
                catch(EntityException)
                {
                    MessageBox.Show("Erreur de connection avec la base de données", "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }
        }

        public static string updatePassword(string oldPW, string newPW)
        {
            Auth auth = sM.currentSession().Auth;
            if (oldPW != auth.motDePasse) return "mot de passe incorrect!";
            using (rent4youEntities model = new rent4youEntities())
            {
                try
                {
                    Auth oldAuth = new Auth();
                    var ori = model.Auth.FirstOrDefault(a => a.motDePasse == oldPW);
                    oldAuth.idAuthentification = ori.idAuthentification;
                    oldAuth.identifiant = auth.identifiant;
                    oldAuth.motDePasse = newPW;
                   

                    model.Entry(ori).CurrentValues.SetValues(oldAuth);
                    model.SaveChanges();
                    return "Mot de passe changer avec succes";
                }
                catch
                {
                    return "Erreur de connection avec la base de données";
                }
            }
        }
    }
}
