﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace rent4you.BU
{
    class SessionManager
    {
        public static Session currentSession()
        {
            try
            {
                using (rent4youEntities model = new rent4youEntities())
                {
                    string mac = getMac();

                    return model.Session.Include(d => d.Auth).Where(s => s.isValid).Where(d => d.macAdress == mac).FirstOrDefault();


                }
            }
            catch { return null; }
        }

        private static String getMac()
        {
            return (
                from nic in NetworkInterface.GetAllNetworkInterfaces()
                where nic.OperationalStatus == OperationalStatus.Up
                select nic.GetPhysicalAddress().ToString()
            ).FirstOrDefault();
        }

        public static void write(int idAuthentification)
        {
            Session newSession = new Session();
            newSession.idAuth = idAuthentification;
            newSession.macAdress = getMac();
            newSession.dateIn = DateTime.Now;
            newSession.isValid = true;
            using (var model = new rent4youEntities())
            {
                model.Session.Add(newSession);
                model.SaveChanges();
            }
        }

        public static void unValidate()
        {
            Session ses = currentSession();
            if (ses != null)
            {
                ses.isValid = false;
                using (var model = new rent4youEntities())
                {
                    model.Session.Attach(ses);
                    model.Entry(ses).State = EntityState.Modified;
                    model.SaveChanges();
                }
            }
        }
    }
}
