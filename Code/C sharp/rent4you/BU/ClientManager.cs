﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Windows;

namespace rent4you.BU
{
    class ClientManager
    {
        public static List<Client> clientList()
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                return model.Client.Include(a => a.Adresse).Include(l => l.Local).Include(al => al.Adresse).ToList();
            }
        }

        public static List<Client> clientListByTypeDeClient(int type)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                return model.Client.Include(a => a.Adresse).Include(l => l.Local).Include(al => al.Adresse).Where(c => c.typeDeClient == type).ToList();
            }
        }

        public static Client getOneById(int id)
        {
            using (var model = new rent4youEntities())
            {
                return model.Client.Include(a => a.Adresse).FirstOrDefault(i => i.idClient == id);
            }
        }

        public static void update(Client client)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                using (var transac = model.Database.BeginTransaction())
                {
                    try
                    {
                        model.Adresse.Attach(client.Adresse);
                        model.Entry(client.Adresse).State = EntityState.Modified;
                        model.SaveChanges();

                        model.Client.Attach(client);
                        model.Entry(client).State = EntityState.Modified;
                        model.SaveChanges();

                        transac.Commit();
                    }
                    catch (Exception er)
                    {
                        transac.Rollback();
                        System.Windows.MessageBox.Show("erreur (again...): " + er);
                    }

                }
            }
           
        }

        public static string insert(Client client)
        {
            int check = AdresseManager.adressExist(client.Adresse);
            using (rent4youEntities model = new rent4youEntities())
            {
                using (var transac = model.Database.BeginTransaction())
                {
                    try
                    {
                        model.Client.Attach(client);

                        if (check == -1) // n'existe pas déja
                        {
                            model.Adresse.Add(client.Adresse);
                            client.idAdresse = client.Adresse.idAdresse;
                        }

                        model.Client.Add(client);
                        model.SaveChanges();
                        transac.Commit();
                    }
                    catch (Exception er)
                    {
                        transac.Rollback();
                        System.Windows.MessageBox.Show("erreur (again...): " + er);
                    }
                }
            }
            return "Nouveau client enregistré";
        }

        public static List<Local> proprietaireLocalList(int id)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                return model.Local.Include(a => a.Adresse).Where(a => a.idProprietaire == id).ToList();
            }
        }

        public static string describeYourself(Client c)
        {
            return c.nom + "\r\n" + c.prenom + "\r\n" + c.telephone + "\r\n" + c.email + "\r\n" + c.typeDeClient;
        }

        public static string describeYourself(int idClient)
        {
            Client c;
            using (var model = new rent4youEntities())
            {
                c = model.Client.Find(idClient);
            }
            return c.nom + "\r\n" + c.prenom + "\r\n" + c.telephone + "\r\n" + c.email + "\r\n" + c.typeDeClient;
        }

    }
}
