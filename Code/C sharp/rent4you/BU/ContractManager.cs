﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
//using System.Windows.Documents;

namespace rent4you.BU
{
    class ContractManager
    {

        public static List<Contrat> contractList()
        {
            using (var model = new rent4youEntities())
            {
                return model.Contrat.Include(c => c.Client).Include(l => l.Local.Adresse).ToList();
            }
        }



        public static string insertNewContrat(Contrat contrat)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                try
                {
                    model.Contrat.Add(contrat);
                    model.SaveChanges();
                    return "Nouveau contrat ajouté avec succes";
                }
                catch (Exception er)
                {
                    //System.Windows.MessageBox.Show("erreur (again...): " + er); //DeBUG
                    return er.Message;
                }
            }
        }

        public static decimal calculateTotalPrice(TimeSpan dateDiff, int idLocal)
        {
            int loyer;
            int forfait;

            using (rent4youEntities model = new rent4youEntities())
            {
                loyer = model.Local.FirstOrDefault(a => a.idlocal == idLocal).loyer;
                forfait = model.Local.FirstOrDefault(a => a.idlocal == idLocal).forfait;
            }

            if (dateDiff.Days > 0)
            {
                return dateDiff.Days * forfait;
            }
            else
            {
                return dateDiff.Hours * loyer;
            }
        }

        public static string describeYourself(Contrat c)
        {
            return "Du " + c.dateDebut + "\n\r au " + c.dateFin + "\r Total € " + c.coutTotal + "\n\r Contrat signé le " + c.dateSignature;
        }

        public static List<string> summary()
        {
            List<string> values = new List<string>();
            List<int> cUp, cNow, cBefore;
            using (var model = new rent4youEntities())
            {
                cUp = model.Contrat.Where(d => d.dateDebut > DateTime.Today).Select(p => p.idContrat).ToList();
                cNow = model.Contrat.Where(d => d.dateDebut == DateTime.Today).Select(p => p.idContrat).ToList();
                cBefore = model.Contrat.Where(d => d.dateFin >= DateTime.Today && d.dateDebut < DateTime.Today).Select(p => p.idContrat).ToList();
            }
            values.Add(cBefore.Count.ToString());
            values.Add(cNow.Count.ToString());
            values.Add(cUp.Count.ToString());
            return values;

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static void generatePDF(Contrat c)
        {
            Document pdfDoc = new Document();
            MemoryStream memoryStream = new MemoryStream();
            Microsoft.Win32.SaveFileDialog path = new Microsoft.Win32.SaveFileDialog();
            path.Title = "Emplacement du contrat à enregistrer en pdf";
            path.FileName = c.Client.nom + " " + c.Client.prenom + " " + (c.dateDebut - c.dateFin).Days.ToString()+ " jours - " + "Contrat.pdf";
            path.Filter = "pdf(*.pdf)|*.pdf";
            path.AddExtension = true;
                
            if (path.ShowDialog().GetValueOrDefault())
            {   
                    
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\PDF\");
                //PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(Directory.GetCurrentDirectory() + @"\PDF\temp.pdf", FileMode.OpenOrCreate));
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, path.OpenFile());

                    
                pdfDoc.Open();
                pdfDoc.Add(new Paragraph("Partie prenante:"));
                pdfDoc.Add(new Paragraph(ClientManager.describeYourself(c.Client)));
                pdfDoc.Add(new Paragraph("Déclare louer le bien:"));
                pdfDoc.Add(new Paragraph(LocalManager.describeYourself(c.Local)));
                pdfDoc.Add(new Paragraph("Du:"));
                pdfDoc.Add(new Paragraph(c.dateDebut.ToLongDateString()));
                pdfDoc.Add(new Paragraph(c.dateDebut.ToLongTimeString()));
                pdfDoc.Add(new Paragraph("Au:"));
                pdfDoc.Add(new Paragraph(c.dateFin.ToLongDateString()));
                pdfDoc.Add(new Paragraph(c.dateFin.ToLongTimeString()));

                pdfDoc.Add(new Paragraph("Total dû à Rent4you:"));
                pdfDoc.Add(new Paragraph(c.coutTotal.ToString() + "€ TTC"));
                PdfContentByte cb = writer.DirectContent;
                //cb.MoveTo(pdfDoc.PageSize.Width / 2, pdfDoc.PageSize.Height / 2);
                //cb.LineTo(pdfDoc.PageSize.Width / 2, pdfDoc.PageSize.Height);
                cb.Stroke();

                pdfDoc.Close();

                //SendToPrinter();
            }
               

        }
        private static void SendToPrinter()
        {
            //var file = File.ReadAllBytes(Directory.GetCurrentDirectory() + @"\PDF\temp.pdf");

            //PrintDialog printDlg = new PrintDialog();

            throw new NotImplementedException();

            //var printQueue = LocalPrintServer.GetDefaultPrintQueue();

            ////PrintDialog pr = new PrintDialog();

            //using (var job = printQueue.AddJob())
            //{
            //    using (var stream = job.JobStream)
            //    {
            //        stream.Write(file, 0, file.Length);
            //    }
            //}
        }
    }
}
