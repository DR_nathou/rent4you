﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Reflection;

namespace rent4you.BU
{
    class LocalManager
    {

        public static List<Local> localList()
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                return model.Local.Include(p => p.Adresse).Include(a => a.Client.Adresse).Where(s => s.suprime == false).ToList();
            }
        }

        public static List<Local> localSupprimeList()
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                return model.Local.Include(p => p.Adresse).Include(a => a.Client.Adresse).Where(s => s.suprime == true).ToList();
            }
        }

        public static void update(Local local)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                using (var transac = model.Database.BeginTransaction())
                {
                    try
                    {

                        model.Adresse.Attach(local.Adresse);
                        model.Entry(local.Adresse).State = EntityState.Modified;
                        model.SaveChanges();

                        model.Local.Attach(local);
                        model.Entry(local).State = EntityState.Modified;
                        model.SaveChanges();

                        transac.Commit();
                    }
                    catch (Exception er)
                    {
                        transac.Rollback();
                        System.Windows.MessageBox.Show("erreur (again...): " + er);
                    }
                }
            }
        }
        
        public static string insert(Local local)
        {
            int check = AdresseManager.adressExist(local.Adresse);

            using (rent4youEntities model = new rent4youEntities())
            {
                using (var transac = model.Database.BeginTransaction())
                {
                    try
                    {
                        model.Local.Attach(local);
                        if (check == -1) // si l'adresse n'existe pas
                        {
                            model.Adresse.Add(local.Adresse);
                            local.idAdresse = local.Adresse.idAdresse;
                        }
                        
                        model.Local.Add(local);
                        model.SaveChanges();

                        var client = model.Client.Find(local.idProprietaire);

                        if (client != null)
                        {
                            client.typeDeClient = 1;
                            model.SaveChanges();
                        }

                        transac.Commit();
                        return "Nouveau local ajouté avec succes appartenant à " + getProprietaireName(local.idProprietaire);
                    }
                    catch (Exception er)
                    {
                        transac.Rollback();
                        //System.Windows.MessageBox.Show("erreur (again...): " + er); //DeBUG
                        return er.ToString();
                    }
                }
            }


        }



        public static void delete(int index, bool toBeDeleted)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                var result = model.Local.SingleOrDefault(b => b.idlocal == index);
                if (result != null)
                {
                    result.suprime = toBeDeleted;
                    model.SaveChanges();
                }
            }
        }


        public static string getProprietaireName(int idProprio)
        {
            using (rent4youEntities model = new rent4youEntities())
            {
                Client c = model.Client.FirstOrDefault(s => s.idClient == idProprio);
                return c.nom + " " + c.prenom;
            }
        }

        public static string describeYourself(Local l)
        {
            return AdresseManager.describeYourself(l.Adresse) + "\r" + l.surface + "\r" + l.loyer + "/heure \n" + l.forfait + "/jour";
        }

        public static string describeYourself(int idLocal)
        {
            Local l;
            using (var model = new rent4youEntities())
            {
                l = model.Local.Include(a => a.Adresse).FirstOrDefault(id => id.idlocal == idLocal);
            }
            return AdresseManager.describeYourself(l.Adresse) + "\r" + l.surface + "\r" + l.loyer + "/heure \n" + l.forfait + "/jour";
        }

    }
}
