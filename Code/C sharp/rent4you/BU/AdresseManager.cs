﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rent4you.BU
{
    class AdresseManager
    {
        /// <summary>
        /// vérifie que l'addresse n'existe pas déjà
        /// </summary>
        /// <param name="adresse"></param>
        /// <returns></returns>
        public static int adressExist(Adresse adresse)
        {
            List<Adresse> adresseList;
            using (rent4youEntities model = new rent4youEntities())
            {
                adresseList = model.Adresse.ToList();
            }


            foreach (var ad in adresseList)
            {
                if ((ad.numero == adresse.numero) && (ad.rue == adresse.rue) &&
                    (ad.ville == adresse.ville) && (ad.codePostal == adresse.codePostal)
                    && ad.pays == adresse.pays)
                {
                    return ad.idAdresse;
                }
            }
            return -1;
        }
        /// <summary>
        /// retourne l'adresse en texte
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static string describeYourself(Adresse a)
        {
            return a.rue + " " + a.numero + " " + a.codePostal + " " + a.ville + " ";
        }

    }
}
