﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace rent4you.BU
{
    class DocumentationManager
    {
        public static string url()
        {
            return (AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["DocUrl"]).Replace(@"\\", @"\");
        }
    }
}
