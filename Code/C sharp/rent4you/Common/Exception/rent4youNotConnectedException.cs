﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace rent4you.Common.Exception
{
    /// <summary>
    /// Exception custom en cas d'essai d'acces aux données necessitant une connection
    /// </summary>
    [Serializable]
    public class rent4youNotConnectedException : System.Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public string fUserName { get; set; }

        /// <summary>
        /// instantiation d'une exception rent4youNotConnectedException
        /// </summary>
        public rent4youNotConnectedException()
        {
        }
        /// <summary>
        /// instantiation d'une exception rent4youNotConnectedException
        /// avec message custom
        /// </summary>
        /// <param name="message"></param>
        public rent4youNotConnectedException(string message) : base(message)
        {
        }
        /// <summary>
        /// instantiation d'une exception rent4youNotConnectedException
        /// avec message custom et
        /// innerException 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public rent4youNotConnectedException(string message,
            System.Exception innerException) : base(message, innerException)
        {
        }
        /// <summary>
        ///  instantiation d'une exception rent4youNotConnectedException
        ///  avec info et 
        ///  context
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected rent4youNotConnectedException(SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            if (info != null)
            {
                this.fUserName = info.GetString("fUserName");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public override void GetObjectData(SerializationInfo info,
            StreamingContext context)
        {
            base.GetObjectData(info, context);

            if (info != null)
            {
                info.AddValue("fUserName", this.fUserName);
            }
        }
        
    }
}
