﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace rent4you
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 
        /// 
        /// </summary>
        public App() : base()
        {
            this.Dispatcher.UnhandledException += Dispatcher_UnhandledException;
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;

            try
            {
                LogException(e.Exception);
            }
            catch
            {

            }
            MessageBox.Show("Erreur! \nLes détails vont être enregistrés et envoyés à Nathanael Smiechowski.\n\r" + e.Exception.Message + e.Exception.InnerException.Message, "erreur", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void LogException(Exception exception)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now + ": ");
            sb.Append(System.Reflection.Assembly.GetExecutingAssembly().ToString() + "- ");
            if (exception == null)
                sb.Append("Unknown error!");
            else
            {
                sb.Append(exception.ToString());
            }

            string errorMsg = sb.ToString();
            // log
            try
            {
                string filePath = ConfigurationManager.AppSettings["LogFilePath"];
                System.IO.Directory.CreateDirectory(filePath);
                System.IO.File.AppendAllText(filePath, errorMsg);
            }
            catch (Exception)
            {

                throw;
            }

            //mail
            try
            {
                SmtpClient client = new SmtpClient();
                MailMessage email = new MailMessage("notification@rent4you.com", "nathan.owski@gmail.com");
                
                client.Port = 465;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("nathan.owski@gmail.com", "spa&ricola");

                email.Subject = "ERROR";
                email.Body = errorMsg;
                email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                client.Send(email);

                //SmtpClient client2 = new SmtpClient();
                //MailMessage email2 = new MailMessage("notification@rent4you.com", "nathan.owski@gmail.com");
                //client2.Host = "aspmx.l.google.com";
                //client2.Port = 25;
                //client2.DeliveryMethod = SmtpDeliveryMethod.Network;
                //email2.Subject = "ERROR";
                //email2.Body = errorMsg;
                //email2.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                //client2.Send(email);


            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
