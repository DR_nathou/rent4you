USE [master]
GO
/****** Object:  Database [rent4you]    Script Date: 22/05/2017 11:39:57 ******/
CREATE DATABASE [rent4you]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'rent4you', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.RENT4YOU\MSSQL\DATA\rent4you.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'rent4you_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.RENT4YOU\MSSQL\DATA\rent4you_log.ldf' , SIZE = 2816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [rent4you].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [rent4you] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [rent4you] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [rent4you] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [rent4you] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [rent4you] SET ARITHABORT OFF 
GO
ALTER DATABASE [rent4you] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [rent4you] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [rent4you] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [rent4you] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [rent4you] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [rent4you] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [rent4you] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [rent4you] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [rent4you] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [rent4you] SET  DISABLE_BROKER 
GO
ALTER DATABASE [rent4you] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [rent4you] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [rent4you] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [rent4you] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [rent4you] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [rent4you] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [rent4you] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [rent4you] SET RECOVERY FULL 
GO
ALTER DATABASE [rent4you] SET  MULTI_USER 
GO
ALTER DATABASE [rent4you] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [rent4you] SET DB_CHAINING OFF 
GO
ALTER DATABASE [rent4you] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [rent4you] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [rent4you]
GO
/****** Object:  Table [dbo].[Adresse]    Script Date: 22/05/2017 11:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Adresse](
	[rue] [varchar](50) NOT NULL,
	[numero] [int] NOT NULL,
	[ville] [varchar](30) NOT NULL,
	[codePostal] [int] NOT NULL,
	[pays] [varchar](25) NULL CONSTRAINT [DF_Adresse_pays]  DEFAULT ('Belgique'),
	[idAdresse] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__adresse__261898460A962C58] PRIMARY KEY CLUSTERED 
(
	[idAdresse] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Auth]    Script Date: 22/05/2017 11:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Auth](
	[idAuthentification] [int] IDENTITY(1,1) NOT NULL,
	[identifiant] [varchar](50) NOT NULL,
	[motDePasse] [varchar](100) NOT NULL,
 CONSTRAINT [PK_auth] PRIMARY KEY CLUSTERED 
(
	[idAuthentification] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Client]    Script Date: 22/05/2017 11:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[nom] [varchar](100) NOT NULL,
	[prenom] [varchar](100) NOT NULL,
	[idNational] [varchar](50) NOT NULL,
	[telephone] [varchar](25) NOT NULL,
	[tva] [varchar](50) NULL,
	[idAdresse] [int] NOT NULL,
	[compteBanquaire] [varchar](120) NULL,
	[www] [varchar](150) NULL,
	[email] [varchar](150) NOT NULL,
	[idClient] [int] IDENTITY(1,1) NOT NULL,
	[typeDeClient] [int] NOT NULL CONSTRAINT [DF_Client_typeDeClient]  DEFAULT ((0)),
 CONSTRAINT [PK__client__A6A610D415BAB8D0] PRIMARY KEY CLUSTERED 
(
	[idClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contrat]    Script Date: 22/05/2017 11:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contrat](
	[dateSignature] [date] NOT NULL,
	[dateDebut] [datetime] NOT NULL,
	[dateFin] [datetime] NOT NULL,
	[idContrat] [int] IDENTITY(1,1) NOT NULL,
	[idClient] [int] NOT NULL,
	[idLocal] [int] NOT NULL,
	[coutTotal] [decimal](18, 0) NULL,
 CONSTRAINT [PK__contrat__2A0DE1E8692F002B] PRIMARY KEY CLUSTERED 
(
	[idContrat] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Local]    Script Date: 22/05/2017 11:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Local](
	[surface] [int] NOT NULL,
	[loyer] [int] NOT NULL,
	[forfait] [int] NOT NULL,
	[meuble] [bit] NOT NULL,
	[description] [varchar](250) NOT NULL,
	[suprime] [bit] NOT NULL,
	[idAdresse] [int] NOT NULL,
	[idProprietaire] [int] NOT NULL,
	[idlocal] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__local__821FE3D7AF27939A] PRIMARY KEY CLUSTERED 
(
	[idlocal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Session]    Script Date: 22/05/2017 11:39:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Session](
	[idSession] [int] IDENTITY(1,1) NOT NULL,
	[dateIn] [datetime] NOT NULL,
	[isValid] [bit] NOT NULL CONSTRAINT [DF__Session__isValid__625A9A57]  DEFAULT ((0)),
	[idAuth] [int] NOT NULL,
	[macAdress] [varchar](20) NOT NULL,
 CONSTRAINT [PK__Session__ADE2668F0C0F0293] PRIMARY KEY CLUSTERED 
(
	[idSession] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_client_adresse] FOREIGN KEY([idAdresse])
REFERENCES [dbo].[Adresse] ([idAdresse])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_client_adresse]
GO
ALTER TABLE [dbo].[Contrat]  WITH CHECK ADD  CONSTRAINT [FK_Contrat_client] FOREIGN KEY([idClient])
REFERENCES [dbo].[Client] ([idClient])
GO
ALTER TABLE [dbo].[Contrat] CHECK CONSTRAINT [FK_Contrat_client]
GO
ALTER TABLE [dbo].[Contrat]  WITH CHECK ADD  CONSTRAINT [FK_Contrat_local] FOREIGN KEY([idLocal])
REFERENCES [dbo].[Local] ([idlocal])
GO
ALTER TABLE [dbo].[Contrat] CHECK CONSTRAINT [FK_Contrat_local]
GO
ALTER TABLE [dbo].[Local]  WITH CHECK ADD  CONSTRAINT [FK_local_adresse] FOREIGN KEY([idAdresse])
REFERENCES [dbo].[Adresse] ([idAdresse])
GO
ALTER TABLE [dbo].[Local] CHECK CONSTRAINT [FK_local_adresse]
GO
ALTER TABLE [dbo].[Local]  WITH CHECK ADD  CONSTRAINT [FK_local_client] FOREIGN KEY([idProprietaire])
REFERENCES [dbo].[Client] ([idClient])
GO
ALTER TABLE [dbo].[Local] CHECK CONSTRAINT [FK_local_client]
GO
ALTER TABLE [dbo].[Session]  WITH CHECK ADD  CONSTRAINT [FK__Session__idAuth__634EBE90] FOREIGN KEY([idAuth])
REFERENCES [dbo].[Auth] ([idAuthentification])
GO
ALTER TABLE [dbo].[Session] CHECK CONSTRAINT [FK__Session__idAuth__634EBE90]
GO
USE [master]
GO
ALTER DATABASE [rent4you] SET  READ_WRITE 
GO
